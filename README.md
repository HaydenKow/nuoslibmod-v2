# nu OldSchool Library (OSLib) MODv2

This is a modded version of OSLib 2.10 and OSLib MOD

The original OSLib by Brunni can be found here:
http://brunni.palib.info/new/index.php?page=pspsoft_oslib

The original OSLib MOD by Sakya can be found here:
http://www.sakya.it/forums/viewforum.php?f=7

# CURRENT RELEASE

- [nu OSLib Modv2](https://gitlab.com/HaydenKow/nuoslibmod-v2/-/releases)

# DOCUMENTATION

You can find the documentation in the Doc directory, or consult it online here:  
http://dogo.github.io/oslibmodv2/

# License

OSLib Modv2 is free software, licensed under the GPLv2; see COPYING for details.

# SUPPORT

You can report bugs and talk about OSLib MODv2 here:  
diautilio@gmail.com (abandoned)

# INSTALL

To install OSLib MODv2
``make install``

# THANKS

mrneo240 for creating the new build system
uppfinnarn for update libpng and makefile  
Strangelove for fixing many bugs  
STAS for tha patch fixing many bugs  
pspZorba for his adhoc sample  
MrMr[iCE] for libpspmath  
InsertWittyName for all the sdk's dialog samples  
Brunni for OSLib  
Sakya for OSLib MOD  
BenHur for intraFont  
