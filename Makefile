#----------------------------------------------------------------------------
#   MAKEFILE
#
#	Nu osLibMod V2
#
#	makefile additions by HaydenKow
#
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
#	Target to make
#	--------------

TARGET_LIB = libosl.a
TARGET := OSLib
PSP_FW_VERSION=500

#----------------------------------------------------------------------------
#	Project folders
#	---------------

SRCDIR := src
BUILD_DIR := build
INCLUDE_DIR := include

INC := -I include

MM_DIR := $(SRCDIR)/mikmod

#----------------------------------------------------------------------------
#	Source to make
#	--------------

SOURCES := $(shell find $(SRCDIR) -name *.c -or -name *.S)
#OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILD_DIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS := $(SOURCES:%=$(BUILD_DIR)/%.o)

#----------------------------------------------------------------------------
#	Additional includes
#	-------------------

INCS :=						-I $(INCLUDE_DIR) \
							-I $(INCLUDE_DIR)/libpspmath \
							-I $(INCLUDE_DIR)/intraFont \
							-I $(INCLUDE_DIR)/adhoc \
							-I $(SRCDIR) \
							-I $(MM_DIR)

#----------------------------------------------------------------------------
#	Preprocesser defines
#	--------------------

DEFINES :=					-DPSP

#----------------------------------------------------------------------------
#	Compiler settings
#	-----------------

CFLAGS :=					$(DEFINES) $(INCS) -O3 -G0 -fomit-frame-pointer -Wall -DHAVE_AV_CONFIG_H
CXXFLAGS :=					$(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS :=					$(CFLAGS)
LDFLAGS :=

#----------------------------------------------------------------------------
#	Default build settings
#	----------------------

PSPSDK :=					$(shell psp-config --pspsdk-path)
PSPDIR :=					$(shell psp-config --psp-prefix)

include						$(PSPSDK)/lib/build.mak

default: $(TARGET_LIB)

$(TARGET_LIB): $(OBJECTS)
	@echo "+ $(TARGET_LIB)"
	@$(MKDIR_P) lib
	@$(AR) rcs lib/$(TARGET_LIB) $^
	@$(RANLIB) lib/$(TARGET_LIB)
	@echo ""; echo "*** SUCCESS ***"

$(BUILD_DIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@$(MKDIR_P) $(dir $@)
	@echo "> $@"
	@$(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo "- clean"
	@$(RM) -r $(BUILD_DIR)

distclean:
	@echo "- distclean"
	@$(RM) -r $(BUILD_DIR) lib/$(TARGET_LIB) libOSLibModV2.tar.gz

lib: $(TARGET_LIB)
	
install: lib
	install -d $(DESTDIR)$(PSPDIR)/lib
	install -m644 lib/$(TARGET_LIB) $(DESTDIR)$(PSPDIR)/lib
	install -d $(DESTDIR)$(PSPDIR)/include/oslib/intraFont/
	install -d $(DESTDIR)$(PSPDIR)/include/oslib/libpspmath/
	install -d $(DESTDIR)$(PSPDIR)/include/oslib/adhoc/
	install -m644 $(INCLUDE_DIR)/intraFont/intraFont.h $(DESTDIR)$(PSPDIR)/include/oslib/intraFont/
	install -m644 $(INCLUDE_DIR)/intraFont/libccc.h $(DESTDIR)$(PSPDIR)/include/oslib/intraFont/
	install -m644 $(INCLUDE_DIR)/libpspmath/pspmath.h $(DESTDIR)$(PSPDIR)/include/oslib/libpspmath/
	install -m644 $(INCLUDE_DIR)/adhoc/pspadhoc.h $(DESTDIR)$(PSPDIR)/include/oslib/adhoc/
	install -m644 $(INCLUDE_DIR)/oslmath.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/net.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/browser.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/audio.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/bgm.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/dialog.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/drawing.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/keys.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/map.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/messagebox.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/osk.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/saveload.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/oslib.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/text.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/usb.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/vfpu_ops.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/VirtualFile.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/vram_mgr.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/ccc.h $(DESTDIR)$(PSPDIR)/include/oslib/
	install -m644 $(INCLUDE_DIR)/sfont.h $(DESTDIR)$(PSPDIR)/include/oslib/

release: lib
	tar -zcvf libOSLibModV2.tar.gz lib include

gendoc:	
	doxygen

ghpages: gendoc
	rm -rf /tmp/ghpages
	mkdir -p /tmp/ghpages
	cp -Rv OSLib_MODv2_Documentation/html/* /tmp/ghpages

	cd /tmp/ghpages && \
		git init && \
		git add . && \
		git commit -q -m "Automatic gh-pages"
	cd /tmp/ghpages && \
		git remote add remote git@github.com:dogo/oslibmodv2.git && \
		git push --force remote +master:gh-pages
	rm -rf /tmp/ghpages

$(BUILD_DIR)/%.S.o: %.S
	@$(MKDIR_P) $(dir $@)
	@$(AS) $(ASFLAGS) -c $< -o $@

$(BUILD_DIR)/%.c.o: %.c
	@echo  "> $@"
	@$(MKDIR_P) $(dir $@)
	@$(CC) $(CPPFLAGS) $(KOS_CFLAGS) $(CFLAGS) $(INCS)  $(LDFLAGS) -c $< -o $@

MKDIR_P ?= mkdir -p
CP_N ?= cp -n
